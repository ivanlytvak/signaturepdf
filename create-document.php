<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/include/check.php');
	$title = "pdf";
    include $_SERVER['DOCUMENT_ROOT'].'include/header.php';
?>
<section class="uk-section  uk-section-xsmall uk-section-muted">
    <div class="uk-container uk-container-large">
        <div class="uk-h3">Create Document</div>
        <form class="create-document" autocomplete="off" enctype="multipart/form-data"  method="post">
            <input type="hidden" name="userid" value="<?=$_SESSION['user_id']?>" />
            <div class="" uk-grid uk-height-match="target: > div > .card-body">
                <div class="uk-width-1-1 uk-width-2-3@m uk-visible@m">
                    <div class="uk-card uk-card-default uk-card-small uk-border-radius-20 uk-card-body card-body">
                        <div>
                            <div class="uk-h5 uk-text-bold">Document info</div>
                            <hr/>
                        </div>
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-1 uk-width-1-2@m">
                                <div class="uk-text-small uk-form-label">Client Name <sup class="uk-text-danger">*</sup></div>
                                <input class="uk-input uk-border-radius-10" name="client_name" type="text" placeholder="" required>
                            </div>
                            <div class="uk-width-1-1 uk-width-1-2@m">
                                <div class="uk-text-small uk-form-label">Client Email <sup class="uk-text-danger">*</sup></div>
                                <input class="uk-input uk-border-radius-10" name="client_email" type="email" placeholder="" required>
                            </div>
                            <div class="uk-width-1-1 uk-width-1-2@m">
                                <div class="uk-text-small uk-form-label">Document name <sup class="uk-text-danger">*</sup></div>
                                <input class="uk-input uk-border-radius-10" name="file_translated_name" type="text" placeholder="" required>
                            </div>
                            <div class="uk-width-1-1 uk-width-1-2@m">
                                <div class="uk-text-small uk-form-label">Number of pages <sup class="uk-text-danger">*</sup></div>
                                <input class="uk-input uk-border-radius-10" name="file_translated_pages" type="number" placeholder="" required>
                            </div>
                            <div class="uk-width-1-1">
                                <div class="uk-text-small uk-form-label">Reference number <sup class="uk-text-danger">*</sup></div>
                                <input class="uk-input uk-border-radius-10" name="reference" type="text" placeholder="" required>
                            </div>
                            <div class="uk-width-1-1 uk-width-1-2@m">
                                <div class="uk-text-small uk-form-label">Source language <sup class="uk-text-danger">*</sup></div>
                                <select class="" id="source-language" name="file_translated_source_language" >
                                    <option></option>
                                    <?
                                    $langlist = mysqli_query($link,"SELECT id, name FROM languages");
                                    while($llist = mysqli_fetch_array($langlist)){?>
                                        <option value="<?=$llist['id']?>"><?=$llist['name']?></option>
                                    <?php } mysqli_data_seek($langlist, 0);?>
                                </select>
                            </div>
                            <div class="uk-width-1-1 uk-width-1-2@m">
                                <div class="uk-text-small uk-form-label">Target language <sup class="uk-text-danger">*</sup></div>
                                <select class="" id="target-language" name="file_translated_target_language" >
                                    <option></option>
                                    <?
                                    $langlist = mysqli_query($link,"SELECT id, name FROM languages");
                                    while($llist = mysqli_fetch_array($langlist)){?>
                                        <option value="<?=$llist['id']?>"><?=$llist['name']?></option>
                                    <?php } mysqli_data_seek($langlist, 0);?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-width-1-3@m">
                    <div class="card-body">
                        <div class="uk-child-width-1-1" uk-grid uk-height-match="target: > div > .uk-card > .uk-card-body">
                            <div>
                                <div class="uk-card uk-card-default uk-card-small uk-border-radius-20">
                                    <div class="uk-card-body">
                                        <div class="uk-grid-small uk-child-width-1-1" uk-grid>
                                            <div>
                                                <div class="uk-h5 uk-text-bold">Template document</div>
                                                <hr/>
                                            </div>
                                            <div>
                                                <div class="uk-text-small uk-form-label">Company <sup class="uk-text-danger">*</sup></div>
                                                <select class="" id="template-country" name="company" >
                                                    <option></option>
                                                    <?
                                                    $cmplist = mysqli_query($link,"SELECT id, company_name FROM company");
                                                    while($clist = mysqli_fetch_array($cmplist)){?>
                                                        <option value="<?=$clist['id']?>"><?=$clist['company_name']?></option>
                                                    <?php } mysqli_data_seek($cmplist, 0);?>
                                                </select>
                                            </div>
                                            <div>
                                                <div class="uk-h5 uk-text-bold">Files</div>
                                                <hr/>
                                            </div>
                                            <div class="uk-width-1-1">
                                                <div class="uk-text-small uk-form-label">Original file <sup class="uk-text-danger">*</sup></div>
                                                <input type="hidden" class="file-original-id" name="file_original_id" value="" required />
                                                <div class="uk-hidden preview-button-original-file" uk-lightbox><a class="uk-button uk-button-text uk-link-preview-original-file uk-margin-bottom" data-caption="Original file" data-type="iframe"><span uk-icon="icon: search; ratio: 0.8;"></span> <span class="original-file-name"></span></a></div>

                                                <div class="original-upload uk-placeholder uk-text-center uk-border-radius-10 uk-margin-remove" id="original-upload">
                                                    <span uk-icon="icon: cloud-upload"></span>
                                                    <span class="uk-text-middle">Attach PDF file or</span>
                                                    <div uk-form-custom>
                                                        <input type="file" name="file_original" multiple>
                                                        <span class="uk-link">selecting one</span>
                                                    </div>
                                                </div>
                                                <progress id="progressbaroriginal" class="uk-progress" value="0" max="100" hidden></progress>
                                            </div>
                                            <div class="uk-width-1-1">
                                                <div class="uk-text-small uk-form-label">Translated file <sup class="uk-text-danger">*</sup></div>
                                                <input type="hidden" class="file-translated-id" name="file_translated_id" value="" required />
                                                <div class="uk-hidden preview-button-translated-file" uk-lightbox><a class="uk-button uk-button-text uk-link-preview-translated-file uk-margin-bottom" data-caption="Translated file" data-type="iframe"><span uk-icon="icon: search; ratio: 0.8;"></span>  <span class="translated-file-name"></span></a></div>
                                                <div class="translated-upload uk-placeholder uk-text-center uk-border-radius-10 uk-margin-remove" id="translated-upload">
                                                    <span uk-icon="icon: cloud-upload"></span>
                                                    <span class="uk-text-middle">Attach PDF file or</span>
                                                    <div uk-form-custom>
                                                        <input type="file" name="file_translated" multiple>
                                                        <span class="uk-link">selecting one</span>
                                                    </div>
                                                </div>
                                                <progress id="progressbartranslated" class="uk-progress" value="0" max="100" hidden></progress>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-text-center uk-text-right@m">
                    <button type="submit" class="uk-button uk-button-danger uk-border-pill">Create document</button>
                </div>
            </div>

        </form>
    </div>
</section>
<? include $_SERVER['DOCUMENT_ROOT'].'include/footer.php';?>