<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/include/check.php');
	$title = "Users list";
	$users = mysqli_query($link,"SELECT * FROM users");
	$user_group = mysqli_query($link,"SELECT * FROM user_groups WHERE id>1 AND active=1");
    include $_SERVER['DOCUMENT_ROOT'].'include/header.php';
?>

		
		<!--AUTHOR-->
		<section class="uk-section uk-section-muted">
			<div class="uk-container uk-container-large">
				<header class="uk-text-left">
					<div class="uk-h2"><span data-uk-icon="icon: users; ratio: 2"></span> Users</div>
				</header>
				<div class="uk-card uk-card-small uk-card-default">
				<div class="uk-card-header">
					<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
						<div class="uk-navbar-left">
							<!--form class="uk-search uk-search-default" action="" method="post">
								<span uk-search-icon></span>
								<input name="search" class="uk-search-input uk-border-pill" type="search" placeholder="Search...">
							</form-->
						</div>
						<div class="uk-navbar-right">
							<a class="uk-button uk-button-danger uk-border-pill" uk-toggle="target: #addusermodal"><span uk-icon="icon: plus"></span> New user</a>
						</div>
					</nav>
				</div>
				
				<div class="uk-card-body">
				<ul uk-tab uk-switcher>
					<?php 
					$ug_x = 0;
					while($ug = mysqli_fetch_array($user_group)) {?>
					<li <?if($ug_x == 0){?>class="uk-active"<?}?>><a href="#"><?=$ug['name']?></a></li>
					<?php $ug_x++; } mysqli_data_seek($user_group, 0);?>

				</ul>
				<ul class="uk-switcher uk-margin">
					<?php
					while($ugs = mysqli_fetch_array($user_group)) {?>
					<li>
						<div class="uk-card-body">
						<table class="uk-table uk-table-striped uk-table-hover uk-table-responsive">
							<thead>
								<tr>
									<th class="uk-table-shrink" style="min-width:20px;"></th>
									<th class="uk-table-shrink"><small>Active</small></th>
									<th class="uk-table-shrink"><small>ID</small></th>
									<th class="uk-table-shrink"><small>Login</small></th>
									<th class="uk-table-shrink"><small>E-mail</small></th>
									<th><small>Comment</small></th>
								</tr>
							</thead>
							<tbody class="uk-text-small">
								<? while($usr = mysqli_fetch_array($users)) {
									if($ugs['id'] == $usr['user_groups']){?>
								<tr>
								  <td class="uk-text-center">
								  <form class="toggle-class" action="/edit-user" autocomplete="off"  method="post">
									<input type="hidden" name="euid" value="<?=$usr['user_id']?>" />
									<button class="uk-button uk-button-link" type="submit"><span class="uk-text-danger" data-uk-icon="icon: file-edit"></span></button>
								  </form>
								 </td>
								  <td><? if($usr['user_active']==1){?><span class="uk-text-success" uk-icon="icon: check"></span><?}else{?><span class="uk-text-danger" uk-icon="icon: close"></span><?}?></td>
								  <td><?=$usr['user_id']?></td>
								  <td><?=$usr['user_login']?></td>
								  <td><?=$usr['user_email']?></td>
								  <td><?=$usr['comment']?></td>
								</tr>
									<?php }} mysqli_data_seek($users, 0);?>

							</tbody>
						</table>
						</div>
						
					</li>
					
					<?php  } mysqli_data_seek($user_group, 0);?>
				</ul>
				</div>
				
				<div class="uk-card-footer">
							<!--ul class="uk-pagination uk-flex-center" uk-margin>
								<li class="uk-active"><span>1</span></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
							</ul-->
						</div>
				</div>
			</div>
		</section>
		
		<!-- add user form modal -->
		
		<div id="addusermodal" class="uk-modal-container" uk-modal>
			<div class="uk-modal-dialog">
			<form class="toggle-class adduserform" action="/include/adduser.php" autocomplete="off"  method="post">
				<button class="uk-modal-close-default" type="button" uk-close></button>
				<div class="uk-modal-header">
					<h2 class="uk-modal-title">Register User</h2>
				</div>
				<div class="uk-modal-body " uk-overflow-auto>
				<div class="uk-grid-small uk-grid-row-medium uk-grid-match" uk-grid>
					<div class="uk-width-1-1">
						<div class="uk-heading-line uk-h3 uk-text-danger uk-text-center"><span>USER INFO</span></div>
					</div>
				</div>
				<fieldset class="uk-fieldset uk-margin-top">
					<div class="uk-card uk-card-small uk-card-default">
							<div class="uk-card-body">
								<div class="uk-grid-small" uk-grid>
                                    <div class="uk-width-1-3@s">
                                        <div class="uk-inline uk-width-1-1">
                                            <span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: user"></span>
                                            <input name="usname" class="uk-input uk-border-pill" required placeholder="Name" type="text">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-3@s">
                                        <div class="uk-inline uk-width-1-1">
                                            <span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: user"></span>
                                            <input name="ussurname" class="uk-input uk-border-pill" required placeholder="Surname" type="text">
                                        </div>
                                    </div>
                                    <div class="uk-width-1-3@s">
						<div class="uk-inline uk-width-1-1">
							<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: user"></span>
							<input name="username" class="uk-input uk-border-pill" required placeholder="Username" type="text">
						</div>
					</div>
					<div class="uk-width-1-3@s">
						<div class="uk-inline uk-width-1-1">
							<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock"></span>
							<input name="password" class="uk-input uk-border-pill" required placeholder="Password" type="password">
						</div>
					</div>
					<div class="uk-width-1-3@s">
						<div class="uk-inline uk-width-1-1">
							<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail"></span>
							<input name="mail" id="mail" class="uk-input uk-border-pill" required placeholder="E-mail" type="mail">
						</div>
					</div>
					<div class="uk-width-1-3@s">
						<div class="uk-inline uk-width-1-1">
							<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail"></span>
							<input name="cfmMail" class="uk-input uk-border-pill" required placeholder="Confirm mail" type="mail">
						</div>
					</div>
					<div class="uk-width-1-3@s">
						<div class="uk-inline uk-width-1-1">
							<select name="usergroups" class="uk-select uk-border-pill select-groups" required>
								<option value="" selected disabled>User groups</option>
								<?php 
									while($ug = mysqli_fetch_array($user_group)) {?>
									<option value="<?=$ug['id']?>" data-name="<?=str_replace(' ', '_', mb_strtolower($ug['name']));?>"><?=$ug['name']?></option>
									<?php } mysqli_data_seek($user_group, 0);?>
							</select>
						</div>
					</div>
								</div>
							</div>
						</div>
					
				</fieldset>
			
				</div>
				<div class="uk-modal-footer">
				<div class="uk-text-right">
						<button type="submit" class="uk-button uk-button-danger uk-border-pill">REGISTER</button>
				</div></div>
			</div>
			</form>
		</div>


<? include $_SERVER['DOCUMENT_ROOT'].'include/footer.php';?>