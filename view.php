<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

$link = "";
require_once($_SERVER['DOCUMENT_ROOT'].'/config/config.php');
$doc = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM documents WHERE uniquelink='".$_GET["s"]."'"));
$clnt = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM clients WHERE id='".$doc['client_id']."'"));
$doc_history = mysqli_fetch_array(mysqli_query($link, "SELECT * FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=2"));
$title = "pdf";

include $_SERVER['DOCUMENT_ROOT'].'/include/header_view.php';

$fname = '';
if($doc['status'] == 1){
    $fname = $doc['merge_pdf'];
}elseif($doc['status'] == 2){
    $fname = str_replace(' ', '_', $clnt['client_name']).'_certificate_'.date("d_m_Y_G_i", strtotime(mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=1"))['date']));
}
?>
<section class="uk-section  uk-section-xsmall uk-section-muted">
    <div class="uk-container uk-container-large">
        <div><p>Lorem ipsum</p></div>
        <div class="" uk-grid uk-height-match="target: > div > .card-body">
            <div class="uk-width-1-1 uk-width-2-3@m uk-visible@m">
                <div class="uk-card uk-card-default uk-card-small uk-border-radius-20 uk-card-body card-body uk-padding-remove">
                    <iframe class="uk-border-radius-20 uk-height-1-1" src="<?='/tmp/'.$doc['dirname'].'/'.$fname.'.pdf'?>#toolbar=0" frameborder="0" width="100%" uk-height-viewport>
                    </iframe>
                </div>
            </div>
            <div class="uk-width-1-1 uk-width-1-3@m">
                <div class="card-body">
                    <div class="uk-child-width-1-1" uk-grid uk-height-match="target: > div > .uk-card > .uk-card-body">
                        <div>
                            <div class="uk-card uk-card-default uk-card-small uk-border-radius-20">
                                <div class="uk-card-header">
                                    <h3 class="uk-card-title uk-text-bold"><span uk-icon="icon: info"></span> Audit log</h3>
                                </div>
                                <div class="uk-card-body">
                                    <div class="uk-grid-small uk-child-width-1-1 uk-grid-divider" uk-grid>
                                        <div>
                                            <div class="uk-h5 uk-text-bold uk-margin-remove-bottom">File Details:</div>
                                            <div><?=str_replace(' ', '_', $clnt['client_name']).'_certificate_'.date("d_m_Y_G_i", strtotime(mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=1"))['date'])).'.pdf'?></div>
                                            <div class="uk-margin-small-top"><a class="uk-button uk-button-danger uk-border-pill" href="<?='/tmp/'.$doc['dirname'].'/'.$fname.'.pdf'?>" download="<?=str_replace(' ', '_', $clnt['client_name']).'_certificate_'.date("d_m_Y_G_i", strtotime(mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=1"))['date'])).'.pdf'?>">Download</a></div>
                                        </div>
                                        <div>
                                            <div class="uk-h5 uk-text-bold uk-margin-remove-bottom">Created</div>
                                            <div class="uk-text-small">by <?php $company = mysqli_fetch_array(mysqli_query($link, "SELECT company_name, mail FROM company WHERE id='".$doc['company']."'"));?> <?="<span class='uk-text-secondary uk-text-bold'>".$company['company_name']."</span> (".$company['mail'].")"?><br/>
                                                <div class="uk-text-emphasis uk-margin-small-top"><span uk-icon="icon:calendar;"></span> <?=date("G:i d M Y", strtotime(mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=1"))['date']))?></div></div>
                                        </div>
                                        <div>
                                            <div class="uk-h5 uk-text-bold uk-margin-remove-bottom">Last Interaction:</div>
                                            <div class="uk-text-small uk-text-emphasis uk-margin-small-top"><span uk-icon="icon:calendar;"></span> <?=date("G:i d M Y", strtotime($doc['update_date']))?></div>
                                        </div>
                                        <div>
                                            <div class="uk-h5 uk-text-bold uk-margin-remove-bottom">Status:</div>
                                            <div class="uk-margin-small-top"><?php $status = mysqli_fetch_array(mysqli_query($link, "SELECT icon, color, name FROM documents_status WHERE id='".$doc['status']."'"));?><div class="uk-text-<?=$status['color']?>"><span uk-icon="icon:<?=$status['icon']?>;ratio:0.8;"></span> <?=$status['name']?></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="uk-h5 uk-text-bold uk-margin-remove-bottom">Document History <span uk-icon="icon: history"></span></div>
                                            <ul class="uk-list uk-margin-auto">
                                                <li>
                                                    <div class="uk-grid-small uk-text-small uk-child-width-expand uk-flex-nowrap uk-grid" uk-grid>
                                                        <div class="uk-width-auto"><span class="uk-text-danger" uk-icon="icon:file-pdf;ratio:1.2;"></span></div>
                                                        <div>
                                                            <span class="uk-text-secondary uk-text-bold"> <?=mysqli_fetch_array(mysqli_query($link, "SELECT company_name FROM company WHERE id='".$doc['company']."'"))['company_name'];?></span> Created the CERTIFICATE
                                                            <div class="uk-margin-small-top"><?=time_elapsed_string(mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=1"))['date'])?></div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <?if($doc['status'] == 2){?>
                                                    <li>
                                                        <div class="uk-grid-small uk-text-small uk-child-width-expand uk-flex-nowrap uk-grid" uk-grid>
                                                            <div class="uk-width-auto"><span class="uk-text-warning" uk-icon="icon:search;ratio:1.2;"></span></div>
                                                            <div>
                                                                <span class="uk-text-secondary uk-text-bold"> <?=mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM reviewer WHERE id='".mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=2"))['user_id']."'"))['name'];?></span> Verified the CERTIFICATE
                                                                <div class="uk-margin-small-top"><?=time_elapsed_string(mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=2"))['date'])?></div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="uk-grid-small uk-text-small uk-child-width-expand uk-flex-nowrap uk-grid" uk-grid>
                                                            <div class="uk-width-auto"><span class="uk-text-success" uk-icon="icon:file-edit;ratio:1.2;"></span></div>
                                                            <div>
                                                                <?php
                                                                $signer = mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM users WHERE user_id='".$doc['creator']."'"));
                                                                ?>
                                                                <span class="uk-text-secondary uk-text-bold"><?=$signer['name'].' '.$signer['surname']?></span> Signed the CERTIFICATE
                                                                <div class="uk-margin-small-top"><?=time_elapsed_string(mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents_history WHERE document_id='".$doc['id']."' AND change_type=3"))['date'])?></div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--div class="uk-width-1-1 uk-text-center uk-text-right@m">
                <button type="submit" class="uk-button uk-button-danger uk-border-pill">Create document</button>
            </div-->
        </div>
    </div>
</section>

<? include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php';?>
