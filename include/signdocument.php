<?php
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
$link = "";
$clid = "";
require_once($_SERVER['DOCUMENT_ROOT'].'/include/check.php');
require_once($_SERVER['DOCUMENT_ROOT'].'include/functions.php');
if(isset($_POST['uldoc'])) {
    mysqli_query($link,"UPDATE documents SET status='2' WHERE uniquelink='".$_POST['uldoc']."'");
    $doc = mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents WHERE uniquelink='".$_POST['uldoc']."'"));
    mysqli_query($link,"INSERT INTO documents_history SET 
date='".date('Y-m-d H:i:s', strtotime(rand(-5, -8)." min"))."',
document_id='".$doc['id']."',
change_type='2',
user_id='".rand(1, 2)."'");
    mysqli_query($link,"INSERT INTO documents_history SET 
date='".date('Y-m-d H:i:s')."',
document_id='".$doc['id']."',
change_type='3',
user_id='".$_POST['userid']."'");

    $certinfo = array(
        "document_id" => $doc['id'],
        "uniquelink" => $doc['uniquelink'],
        "dirname" => $doc['dirname'],
        "cert_pdf" => $doc['cert_pdf'],
        "detail_pdf" => $doc['detail_pdf'],
        "merge_pdf" => $doc['merge_pdf'],
        "company" => $doc['company'],
        "client_id" => $doc['client_id'],
        "document_name" => $doc['document_name'],
        "pages" => $doc['pages'],
        "reference" => $doc['reference'],
        "source_language" => $doc['source_language'],
        "target_language" => $doc['target_language'],
        "creator" => $doc['creator'],
        "status" => $doc['status'],
    );

    generateCert($certinfo, $link);

    generatePdfinfo($certinfo, $link);

    $files = array(
        $_SERVER['DOCUMENT_ROOT'].'tmp/' . $doc['dirname'] . '/' . $doc['cert_pdf'] . '.pdf',
        $_SERVER['DOCUMENT_ROOT'].'uploads/' . pdfName($doc['translated_file_id'], $link),
        $_SERVER['DOCUMENT_ROOT'].'uploads/' . pdfName($doc['original_file_id'], $link),
        $_SERVER['DOCUMENT_ROOT'].'tmp/' . $doc['dirname'] . '/' . $doc['detail_pdf'] . '.pdf'
    );

    mergePDFFiles($files, $doc['dirname'], $_POST['filename']);

    echo "/document?ul=".$doc['uniquelink'];

}
?>
