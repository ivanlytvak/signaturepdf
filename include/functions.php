<?php
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
require_once($_SERVER['DOCUMENT_ROOT'].'/include/check.php');
require_once($_SERVER['DOCUMENT_ROOT'].'vendor/autoload.php');
use setasign\Fpdi\Fpdi;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;
function generateCode($length=6) {
    $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function pdfName($idp=0, $lnk){
    return mysqli_fetch_array(mysqli_query($lnk, "SELECT newname FROM files WHERE id='".$idp."'"))['newname'];
}

function generatorQr($data){
    $options = new QROptions(
        [
            'eccLevel' => QRCode::ECC_L,
            'outputType' => QRCode::OUTPUT_MARKUP_SVG,
            'version' => 5,
        ]
    );
    return (new QRCode($options))->render($data);
}

function mergePDFFiles(Array $filenames, $dirname, $outFile) {
    $mpdf = new \Mpdf\Mpdf();
    if ($filenames) {
        $filesTotal = sizeof($filenames);
        $fileNumber = 1;
        foreach ($filenames as $fileName) {
            if (file_exists($fileName)) {
                $pagesInFile = $mpdf->SetSourceFile($fileName);
                for ($i = 1; $i <= $pagesInFile; $i++) {
                    if ($i <= $pagesInFile) {
                    $tplId = $mpdf->ImportPage($i);
                    $size = $mpdf->getTemplateSize($tplId);
                    $mpdf->AddPage($size['orientation']);
                    $mpdf->useTemplate($tplId, 0, 0, $size['width'], $size['height'], true);
                    }
                }
            }
            $fileNumber++;
        }
        $mpdf->Output($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . $dirname . '/' . $outFile . '.pdf', 'F');
    }
}

function generateCert(Array $data, $dblink){
    $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    $fontDirs = $defaultConfig['fontDir'];
    $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    $fontData = $defaultFontConfig['fontdata'];
    $mpdf = new \Mpdf\Mpdf(
        [
            'fontDir' => array_merge($fontDirs, [
                $_SERVER['DOCUMENT_ROOT'] . '/fonts',
            ]),
            'fontdata' => $fontData + [
                    'centurygothic' => [
                        'R' => 'centurygothic.ttf',
                        'B' => 'centurygothic_bold.ttf',
                    ],
                    'coralf' => [
                        'R' => 'Coral_W_Black.ttf',
                    ],
                    'courier' => [
                        'R' => 'CourierPrime_Regular.ttf',
                        'B' => 'CourierPrime_Bold.ttf',
                    ]
                ],
            'default_font' => 'centurygothic'
        ]
    );
    $pidpys = '';
    $qr = '';
    $signdate = '';
    $company = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM company WHERE id='".$data['company']."'"));
    $signer = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM users WHERE user_id='".$data['creator']."'"));
    $createdate = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM documents_history WHERE document_id='".$data['document_id']."' AND change_type=1"))['date'];
    if($data['status'] == 2){
    $signdate =  mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM documents_history WHERE document_id='".$data['document_id']."' AND change_type=3"))['date'];
    $qr = '<img width="80px" src="'.generatorQr('https://'.$_SERVER['SERVER_NAME'].'/view?s='.$data['uniquelink']).'"/>';
    $pidpys = '
    <span style="font-family: courier; font-weight:bold; font-size:9px;">
    '.mb_substr($signer['name'], 0, 1).' '.$signer['surname'].'<br/>
    '.$signdate.'
    </span>
    <div style="font-size: 15px; font-family: coralf; margin-top:5px;">'.$signer['name'].' '.$signer['surname'].'</div>
    <br/><br/><br/>';
    }else{$qr = '';$pidpys = "<br/><br/><br/><br/><br/><br/>";}
    $date = sprintf("Date: <b>%s</b>", date("d/m/Y ", strtotime($createdate)));
    if($data['pages'] > 1){$mn = "s";}
    $dirname = $data['dirname'];
    $filename = $data['cert_pdf'];
    $company_name = $company['company_name'];
    $company_adress = sprintf("Address: %s", $company['adress']);
    $company_tel = sprintf("Tel: %s", $company['tel']);
    $company_mail = sprintf("Email: %s", $company['mail']);
    $company_website = sprintf("Web: %s", $company['website']);
    $company_info = $company['info'];
    $stamp = $company['stamp'];
    $document_name = sprintf("File translated: <b>%s</b>", $data['document_name']);
    $pages = sprintf("Consisting of: <b>%s page%s</b>", $data['pages'], $mn);
    $reference = sprintf("Reference: <b>%s</b>", $data['reference']);
    $slang = sprintf("Source Language: <b>%s</b>", mysqli_fetch_array(mysqli_query($dblink, "SELECT name  FROM languages WHERE id='".$data['source_language']."'"))['name']);
    $tlang = sprintf("Target Language: <b>%s</b>", mysqli_fetch_array(mysqli_query($dblink, "SELECT name  FROM languages WHERE id='".$data['target_language']."'"))['name']);

    $html = '<html>
<head>
<style>
    @page {
        size: auto;
        margin: 0;
        margin-top: 170px;
        margin-header: 0px;
        margin-footer: 0px;
        odd-header-name: html_myHeader;
        odd-footer-name: html_myFooter;
    }
</style>
</head>
<body>
    <htmlpageheader name="myHeader" style="display:none">
        <div>
            <img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/header_pdf.jpg" style="width:100%" />
        </div>
    </htmlpageheader>
    <htmlpagefooter name="myFooter" style="display:none">
        <div>
            <img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/footer_pdf.jpg" style="width:100%" />
        </div>
    </htmlpagefooter>';

    $html .= '<div style="padding-left: 40px; padding-right: 40px;">';
    $html .= '
    <table width="100%" border="0" style="border:0px;">
    <tr>
    <td width="60%"></td>
    <td width="35%">
        <table width="100%" border="0" style="border:0px; font-size:10px;">
    <tr>
    <td width="30px" align="center"><img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/location.svg" /></td><td>'.$company_adress.'</td>
    </tr><tr>
    <td width="30px" align="center"><img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/phone.svg" /></td><td>'.$company_tel.'</td>
    </tr><tr>
    <td width="30px" align="center"><img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/mail.svg" /></td><td>'.$company_mail.'</td>
    </tr><tr>
    <td width="30px" align="center"><img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/web.svg" /></td><td>'.$company_website.'</td>
    </tr><tr>
    <td width="30px"></td><td>'.$company_info.'</td>
    </tr>
    </table>
</td>
<td></td>
</tr>
        </table>';
    $html .= '<div style=" margin-top: 30px; font-size:18px; text-align:center;color:#184184;"><b>TRANSLATION CERTIFICATE</b></div>';
    $html .= '<div style=" margin-top: 10px; font-size:12px;">I, the undersigned '.$signer['name'].' '.$signer['surname'].', '.$company_name.', do hereby certify that the enclosed translation of the source document described below was produced in accordance with our Quality Management System, complies with our Code of Practice , and has been validated and judged to be a true and accurate translation of an original document.</div>';
    $html .= '<div style="margin-top:10px;font-size:12px;">'.$date.'<div>';
    $html .= '<div style="margin-top:10px;font-size:12px;">'.$document_name.'<div>';
    $html .= '<div style="margin-top:10px;font-size:12px;">'.$pages.'<div>';
    $html .= '<div style="margin-top:10px;font-size:12px;">'.$reference.'<div>';
    $html .= '<div style="margin-top:10px;font-size:12px;">'.$slang.'<div>';
    $html .= '<div style="margin-top:10px;font-size:12px;">'.$tlang.'<div>';
    $html .= '<div style="margin-top:20px;font-size:12px;">We declare that the agents responsible for said translations are qualified to translate and review documents for the above language pair and are not related to any of the parties named in the source documents. We do not guarantee that the original is a genuine document or that the statements contained in the original document(s) are true.<div>';
    //$html .= '<div style="margin-top:20px;font-size:12px;">'.$img.'<div>';
    $html .= '
    <table width="100%" border="0" style="border:0px; margin-top:150px;">
    <tr>
    <td width="50%">
    <img style="margin-left:60px; transform: rotate('.rand(-30, 30).'deg);" width="150px" src="'.$_SERVER["DOCUMENT_ROOT"].'/images/pdf/'.$stamp.'" />
</td>
    <td width="50%">
    <div>
    '.$pidpys.'
    </div>
    <table width="100%" border="0" style="border:0px;">
    <tr>
    <td width="90px"><a href="https://'.$_SERVER['SERVER_NAME']."/view?s=".$data['uniquelink'].'">'.$qr.'</a></td>
    <td>
    <div style="font-size:12px; text-align:center;color:#184184;"><b>ORIGINAL CERTIFIED TRANSLATION</b></div>
<div style="font-size:10px;">Scan the QR code to check your digital signed copy against the original translation prepared by Docsbase, in order to verify the authenticity of the document.</div>
</td>
</tr>
    </table>
</td>
    </tr>
    </table>
    ';
    $html .= '</div>';
    $html .= '</body></html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . $dirname . '/' . $filename . '.pdf',
        'F');
}

function generatePdfinfo(Array $data, $dblink){
    $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
    $fontDirs = $defaultConfig['fontDir'];
    $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
    $fontData = $defaultFontConfig['fontdata'];
    $mpdf = new \Mpdf\Mpdf(
        [
            'fontDir' => array_merge($fontDirs, [
                $_SERVER['DOCUMENT_ROOT'] . '/fonts',
            ]),
            'fontdata' => $fontData + [
                    'centurygothic' => [
                        'R' => 'centurygothic.ttf',
                        'B' => 'centurygothic_bold.ttf',
                    ],
                    'coralf' => [
                        'R' => 'Coral_W_Black.ttf',
                    ],
                    'courier' => [
                        'R' => 'CourierPrime_Regular.ttf',
                        'B' => 'CourierPrime_Bold.ttf',
                    ]
                ],
            'default_font' => 'centurygothic'
        ]
    );

    $company = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM company WHERE id='".$data['company']."'"));
    $reviewer = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM reviewer WHERE id='".mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM documents_history WHERE document_id='".$data['document_id']."' AND change_type=2"))['user_id']."'"))['name'];
    $signer = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM users WHERE user_id='".$data['creator']."'"));
    $createdate = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM documents_history WHERE document_id='".$data['document_id']."' AND change_type=1"))['date'];
    $reviewdate = mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM documents_history WHERE document_id='".$data['document_id']."' AND change_type=2"))['date'];
    $signdate =  mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM documents_history WHERE document_id='".$data['document_id']."' AND change_type=3"))['date'];
    $clnt = mysqli_fetch_array(mysqli_query($dblink, "SELECT * FROM clients WHERE id='".$data['client_id']."'"));
    $qr = '<img width="150px" src="'.generatorQr("https://".$_SERVER["SERVER_NAME"]."/view?s=".$data["uniquelink"]).'"/>';
    $digitalfingerprint = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $reviewfingerprint = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $signedfingerprint = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $dirname = $data['dirname'];
    $filename = $data['detail_pdf'];
    $company_name = $company['company_name'];
    $company_mail = sprintf("(%s)", $company['mail']);
    $stamp = $company['stamp'];

    $html = '<html>
<head>
<style>
    @page {
        size: auto;
        margin: 0;
        margin-top: 170px;
        margin-header: 0px;
        margin-footer: 0px;
        odd-header-name: html_myHeader;
        odd-footer-name: html_myFooter;
    }
    td {border-bottom: 1px solid #e7e7e7; padding: 7px; padding-left: 0px;}
</style>
</head>
<body>
    <htmlpageheader name="myHeader" style="display:none">
        <div>
            <img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/header_pdf.jpg" style="width:100%" />
        </div>
    </htmlpageheader>
    <htmlpagefooter name="myFooter" style="display:none">
        <div>
            <img src="'.$_SERVER["DOCUMENT_ROOT"].'/images/footer_pdf.jpg" style="width:100%" />
        </div>
    </htmlpagefooter>';

    $html .= '<div style=" margin-top: 10px; padding-left: 40px; padding-right: 40px;">';
    $html .= '<div style="font-size: 10px;">This Document has been Signed with a secure electronic signature via <b>'.$company_name.'</b></div>';
    $html .= '<div style=" margin-top: 30px; font-size:18px; color:#184184;"><b>DOCUMENT DETAIL</b></div>';
    $html .= '
    <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-top: 20px; border:0px; font-size: 12px;">
        <tr>
            <td width="200px"><b>Title</b></td>
            <td>' .str_replace(' ', '_', $clnt['client_name']).'_certificate_'.date("d_m_Y_G_i", strtotime(mysqli_fetch_array(mysqli_query($dblink, "SELECT *  FROM documents_history WHERE document_id='".$data['document_id']."' AND change_type=1"))['date'])).'.pdf'.'</td>
        </tr>
        <tr>
            <td width="200px"><b>Author</b></td>
            <td><b>'.$company_name.'</b> '.$company_mail.'</td>
        </tr>
        <tr>
            <td width="200px"><b>Document Created on</b></td>
            <td>'.date("d M Y, H:i:s  ", strtotime($createdate)).'</td>
        </tr>
        <tr>
            <td width="200px"><b>Digital Fingerprint</b></td>
            <td>'.$reviewfingerprint.'</td>
        </tr>
    </table>
';
    $html .= '<div style=" margin-top: 30px; font-size:18px; color:#184184;"><b>DOCUMENT REVIEWER</b></div>';
    $html .= '
    <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-top: 20px; border:0px; font-size: 12px;">
        <tr>
            <td width="200px"><b>Name</b></td>
            <td>' . $reviewer . '</td>
        </tr>
        <tr>
            <td width="200px"><b>Status</b></td>
            <td><b>Verified</b> at '.date("d M Y, H:i:s  ", strtotime($reviewdate)).'</td>
        </tr>
        <tr>
            <td width="200px"><b>Digital Fingerprint</b></td>
            <td>'.$digitalfingerprint.'</td>
        </tr>
    </table>
';
    $html .= '<div style=" margin-top: 30px; font-size:18px; color:#184184;"><b>DOCUMENT SIGNER</b> <span style="font-size: 9px;">Scan/Click the QR Code to view signature information</span></div>';
    $html .= '
    <table border="0" width="100%" cellspacing="0" cellpadding="0" style="margin-top: 20px; border:0px; padding:0px;">
    <tr><td style="border-bottom: 0px; padding:0px;">
    <table border="0" width="100%" cellspacing="0" cellpadding="0" style=" border:0px; font-size: 12px;">
        <tr>
            <td width="200px"><b>Name</b></td>
            <td>' . $signer['name'] . ' ' . $signer['surname'] . '</td>
        </tr>
        <tr>
            <td width="200px"><b>Email</b></td>
            <td>'.$signer['user_email'].'</td>
        </tr>
        <tr>
            <td width="200px"><b>Status</b></td>
            <td><b>Signed</b> at '.date("d M Y, H:i:s  ", strtotime($signdate)).'</td>
        </tr>
        <tr>
            <td width="200px"><b>Digital Fingerprint</b></td>
            <td>'.$signedfingerprint.'</td>
        </tr>
    </table></td>
    <td width="150px" style="border-bottom: 0px; padding:0px;"><a href="https://'.$_SERVER['SERVER_NAME']."/view?s=".$data['uniquelink'].'">' . $qr . '</a></td></tr></table>
';
    $html .= '
    <table width="100%" border="0" style="border:0px; margin-top:30px;">
    <tr>
    <td width="100%" style="border-bottom: 0px; padding:0px;">
    <img style="margin-left:60px; transform: rotate('.rand(-30, 30).'deg);" width="150px" src="'.$_SERVER["DOCUMENT_ROOT"].'/images/pdf/'.$stamp.'" />
</td></tr></table>
';
    $html .= '</div>';
    $html .= '</body></html>';
    $mpdf->WriteHTML($html);
    $mpdf->Output($_SERVER['DOCUMENT_ROOT'] . '/tmp/' . $dirname . '/' . $filename . '.pdf',
        'F');
}

function sendAdminMail() {
    $transport = (new Swift_SmtpTransport('smtp-relay.gmail.com', 465, 'ssl'));

// Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);

// Create a message
    $message = (new Swift_Message('Wonderful Subject'))
        ->setFrom(['no-reply@certifiedtranslations.ie' => 'Certified Translations'])
        ->setTo(['litvakivan88@gmail.com'])
        ->setBody('Here is the message itself')
    ;

// Send the message
    $mailer->send($message);
}
sendAdminMail();
?>
