<?php
$link = "";
$clid = "";
require_once($_SERVER['DOCUMENT_ROOT'].'/include/check.php');
require_once($_SERVER['DOCUMENT_ROOT'].'include/functions.php');
if(isset($_POST['file_translated_name'])) {
    $uniquelink = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $dirname = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $cert_pdf = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $detail_pdf = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $merge_pdf = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12);
    $clientid = mysqli_fetch_array(mysqli_query($link, "SELECT id FROM clients WHERE client_email='".$_POST['client_email']."'"))['id'];
    if(empty($clientid)){
        mysqli_query($link,"INSERT INTO clients SET client_name='".$_POST['client_name']."', client_email='".$_POST['client_email']."'");
        $clid = mysqli_fetch_array(mysqli_query($link, "SELECT id FROM clients WHERE client_email='".$_POST['client_email']."'"))['id'];
    }else {$clid = $clientid;}
mysqli_query($link,"INSERT INTO documents SET 
uniquelink='".$uniquelink."',
dirname='".$dirname."',
cert_pdf='".$cert_pdf."',
detail_pdf='".$detail_pdf."',
merge_pdf='".$merge_pdf."',
company='".$_POST['company']."',
client_id='".$clid."',
document_name='".$_POST['file_translated_name']."',
pages='".$_POST['file_translated_pages']."',
reference='".$_POST['reference']."',
source_language='".$_POST['file_translated_source_language']."',
target_language='".$_POST['file_translated_target_language']."',
original_file_id='".$_POST['file_original_id']."',
translated_file_id='".$_POST['file_translated_id']."',
creator='".$_POST['userid']."'");
    if(!is_dir($_SERVER['DOCUMENT_ROOT'].'/tmp/'.$dirname)) {
        mkdir($_SERVER['DOCUMENT_ROOT'].'/tmp/'.$dirname, 0777, true);
    }
    $doc = mysqli_fetch_array(mysqli_query($link, "SELECT *  FROM documents WHERE uniquelink='".$uniquelink."'"));

    mysqli_query($link,"INSERT INTO documents_history SET 
date='".date('Y-m-d H:i:s', strtotime(rand(-20, -9)." min"))."',
document_id='".$doc['id']."',
change_type='1',
user_id='".$_POST['userid']."'");
    $certinfo = array(
        "document_id" => $doc['id'],
        "uniquelink" => $uniquelink,
        "dirname" => $dirname,
        "cert_pdf" => $cert_pdf,
        "detail_pdf" => $detail_pdf,
        "merge_pdf" => $merge_pdf,
        "company" => $_POST['company'],
        "client_id" => $clid,
        "document_name" => $_POST['file_translated_name'],
        "pages" => $_POST['file_translated_pages'],
        "reference" => $_POST['reference'],
        "source_language" => $_POST['file_translated_source_language'],
        "target_language" => $_POST['file_translated_target_language'],
        "creator" => $_POST['userid'],
        "status" => $doc['status'],
    );

    generateCert($certinfo, $link);
    $files = array(
        $_SERVER['DOCUMENT_ROOT'].'tmp/' . $dirname . '/' . $cert_pdf . '.pdf',
        $_SERVER['DOCUMENT_ROOT'].'uploads/' . pdfName($_POST['file_translated_id'], $link),
        $_SERVER['DOCUMENT_ROOT'].'uploads/' . pdfName($_POST['file_original_id'], $link)
    );
    mergePDFFiles($files, $dirname, $merge_pdf);

}
echo "/document?ul=".$uniquelink;
?>
