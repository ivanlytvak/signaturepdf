<?php
$link = "";
require_once('check.php');

function generateCode($length=6) {
    $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}
//8edb43a0-f767-4694-9b0c-4b045dcf59a1
//8-4-4-4-12
if($_FILES['file_original']["name"] != ''){
    $filename = $_FILES["file_original"]["name"];
    $tmp_name = $_FILES["file_original"]["tmp_name"];
    $newpdfname = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12).'.pdf';
    $uploads_dir = $_SERVER['DOCUMENT_ROOT'].'uploads/';
    $file = $uploads_dir.$newpdfname;
}
if($_FILES['file_translated']["name"] != ''){
    $filename = $_FILES['file_translated']["name"];
    $tmp_name = $_FILES['file_translated']["tmp_name"];
    $newpdfname = generateCode(8).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(4).'-'.generateCode(12).'.pdf';
    $uploads_dir = $_SERVER['DOCUMENT_ROOT'].'uploads/';
    $file = $uploads_dir.$newpdfname;
}

if (move_uploaded_file($tmp_name, $file) ) {
    mysqli_query($link,"INSERT INTO files SET originalname='".addslashes($filename)."',newname='".addslashes($newpdfname)."'");
    $idfile = mysqli_fetch_array(mysqli_query($link, "SELECT id FROM files WHERE newname='".$newpdfname."'"))['id'];
    $arr = array('originalfname' => $filename,'newfname' => $newpdfname, 'id' => $idfile);
    echo json_encode($arr);
} else {
    die();
}
?>
