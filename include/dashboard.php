<?php
$docs = mysqli_query($link, "SELECT * FROM documents WHERE creator='".$_SESSION['user_id']."' ORDER BY `id` desc");
?>
<div class="uk-container uk-container-large">
    <div class="uk-h3">Dashboard</div>
    <div class="" uk-grid uk-height-match="target: > div > .card-body">
        <div class="uk-width-1-1 uk-width-1-4@m">
            <div class="card-body">
                <div class="uk-child-width-1-1" uk-grid uk-height-match="target: > div > .uk-card > .uk-card-body">
                    <div>
                        <div class="uk-card uk-card-default uk-card-small uk-border-radius-20">

                            <div class="uk-card-body uk-text-center">
                                <div>
                                    <span class="uk-text-primary" uk-icon="icon:push; ratio: 4;" ></span>
                                </div>
                                <div class="uk-h3 uk-margin-remove-bottom uk-margin-top">New Signature Workflow</div>
                                <div class="uk-h5 uk-margin-remove-top">Sign or send a document</div>
                                <div>
                                    <div class="uk-margin-small-top"><a class="uk-button uk-button-default uk-button-small uk-border-radius-20" href="/create-document" >Get Started</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card uk-card-default uk-card-small uk-border-radius-20">
                            <div class="uk-card-body uk-text-center">
                                <div class="uk-h3">Document Status</div>
                                <div>
                                    <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-text-center uk-grid-divider uk-grid-small" uk-grid>
                                        <div>
                                            <div>
                                                <span class="uk-text-success" uk-icon="icon:check; ratio: 2;" ></span>
                                            </div>
                                            <div><?php
                                                print_r(mysqli_fetch_array(mysqli_query($link,"SELECT count(*) as total from documents where creator='".$_SESSION['user_id']."' and status=2"))['total']);
                                                ?></div>
                                            <div>Completed</div>
                                            <div class="uk-margin-small-top"><a class="uk-button uk-button-default uk-button-small uk-border-radius-20" href="" >View all</a></div>
                                        </div>
                                        <div>
                                            <div>
                                                <span class="uk-text-warning" uk-icon="icon:clock; ratio: 2;" ></span>
                                            </div>
                                            <div><?php
                                                print_r(mysqli_fetch_array(mysqli_query($link,"SELECT count(*) as total from documents where creator='".$_SESSION['user_id']."' and status=1"))['total']);
                                                ?>
                                                </div>
                                            <div>Pending</div>
                                            <div class="uk-margin-small-top"><a class="uk-button uk-button-default uk-button-small uk-border-radius-20" href="" >View all</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-1-1 uk-width-3-4@m uk-visible@m">
            <div class="uk-card uk-card-default uk-card-small uk-border-radius-20 uk-card-body card-body">
                <div class="uk-h4">Documents</div>
                <div>
                    <table class="uk-table uk-table-small uk-table-striped uk-table-middle">
                        <thead>
                        <tr>
                            <th class="uk-table-shrink">№</th>
                            <th>Company</th>
                            <th>Document</th>
                            <th>Client</th>
                            <th>Status</th>
                            <!--th>Signer</th-->
                            <th class="uk-table-shrink"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        while($doc = mysqli_fetch_array($docs)) {?>
                        <tr>
                            <td><?=$doc['id']?></td>
                            <td><?=mysqli_fetch_array(mysqli_query($link, "SELECT company_name FROM company WHERE id='".$doc['company']."'"))['company_name'];?></td>
                            <td><?=$doc['document_name']?></td>
                            <td><?=mysqli_fetch_array(mysqli_query($link, "SELECT client_name FROM clients WHERE id='".$doc['client_id']."'"))['client_name'];?></td>
                            <td><?php $status = mysqli_fetch_array(mysqli_query($link, "SELECT icon, color, name FROM documents_status WHERE id='".$doc['status']."'"));?><div class="uk-text-<?=$status['color']?>"><span uk-icon="icon:<?=$status['icon']?>;ratio:0.8;"></span> <?=$status['name']?></div></td>
                            <!--td><?/*php $signer = mysqli_fetch_array(mysqli_query($link, "SELECT name, surname FROM users WHERE user_id='".$doc['creator']."'"));?><?=$signer['name']." ".$signer['surname']*/?></td-->
                            <td><a class="uk-button uk-button-default uk-button-small uk-border-radius-20" href="/document?ul=<?=$doc['uniquelink']?>">Open</a></td>
                        </tr>
                        <?php } mysqli_data_seek($docs, 0);?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>