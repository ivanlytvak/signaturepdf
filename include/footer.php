</div>
<!--FOOTER-->
<footer class="uk-section-secondary">
    <div class="uk-section uk-section-xsmall" style="background-color: rgba(0,0,0,0.15)">
        <div class="uk-container">
            <div class="uk-grid uk-child-width-1-1@m uk-text-center" data-uk-grid>
                <div class="uk-text-small uk-text-muted">
                    Copyright © <?=date('Y');?> - All rights reserved. Docsbase
                </div>
                <!--div class="uk-text-small uk-text-muted uk-text-center uk-text-right@m">
                    Created by <a href="https://www.facebook.com/lytvak" target="_blank">Ivan Lytvak</a>
                </div-->
            </div>
        </div>
    </div>
</footer>
<!--/FOOTER-->
<!-- JS FILES -->
<script src="/js/jquery-3.4.1.min.js"></script>
<!-- UIkit JS -->
<script src="https://cdn.jsdelivr.net/npm/uikit@3.10.1/dist/js/uikit.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/uikit@3.10.1/dist/js/uikit-icons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.3/js/standalone/selectize.min.js"></script>
<script src="/js/jquery.validate.min.js"></script>
<script src="/js/datepicker.js"></script>
<script src="/js/scripts.js"></script>
	</body>
</html>