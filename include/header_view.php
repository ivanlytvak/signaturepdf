<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$title;?> | PDF</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
        <link rel="manifest" href="/favicons/site.webmanifest">
        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/favicons/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/favicons/mstile-144x144.png">
        <meta name="msapplication-config" content="/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
		<!-- CSS FILES -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.10.1/dist/css/uikit.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/flag-icon.min.css">
		<link rel="stylesheet" type="text/css" href="/css/datepicker.css">
		<link rel="stylesheet" type="text/css" href="/css/selectize.css">
		<link rel="stylesheet" type="text/css" href="/css/style.css">
	</head>
	<body class="uk-background-muted">
	<div class="se-pre-con uk-flex uk-width-1-1 uk-flex-middle">
			<div class="uk-text-danger uk-width-1-1 uk-text-center" uk-spinner="ratio: 2"></div>
	</div>
		<!-- OFFCANVAS -->
		<div id="offcanvas-nav" data-uk-offcanvas="flip: true; overlay: true">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
				<button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>
				<img src="/images/logo-white.svg" class="uk-margin-top" width="150px" />
				<ul class="uk-nav uk-nav-default uk-margin-top uk-nav-parent-icon" uk-nav="multiple: true">
					<li><a href="/">Home</a></li>
					<?/*if($_SESSION['user_groups']=='4'){?><li><a href="/create-order" class="uk-text-danger">Create order</a></li><?}*/?>

				</ul>
			</div>
		</div>
		<!-- /OFFCANVAS -->
		<!--HEADER-->
		<header class="uk-box-shadow-small" style="background-color: white">
			<div class="uk-container uk-container-expand">
				<nav class="uk-navbar" id="navbar" data-uk-navbar>
					<div class="uk-navbar-left">
						<a class="uk-navbar-item uk-logo" href="/"><img src="/images/logo.svg"/></a>
					</div>
					
					<div class="uk-navbar-right">
						<!--ul class="uk-navbar-nav uk-visible@m">
							<li><a href="/"><span uk-icon="icon: home; ratio: 0.8;"></span> Dashboard</a></li>
                            <li><a href="/create-document"><span uk-icon="icon: push; ratio: 0.8;"></span> Create Document</a></li>
						</ul-->

					</div>
				</nav>
			</div>
		</header>
		<!--/HEADER-->
    <div class="main-area">