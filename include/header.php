<!DOCTYPE html> 
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?=$title;?> | PDF</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
        <link rel="manifest" href="/favicons/site.webmanifest">
        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/favicons/favicon.ico">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/favicons/mstile-144x144.png">
        <meta name="msapplication-config" content="/favicons/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
		<!-- CSS FILES -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.10.1/dist/css/uikit.min.css" />
		<link rel="stylesheet" type="text/css" href="/css/flag-icon.min.css">
		<link rel="stylesheet" type="text/css" href="/css/datepicker.css">
		<link rel="stylesheet" type="text/css" href="/css/selectize.css">
		<link rel="stylesheet" type="text/css" href="/css/style.css">
	</head>
	<body class="uk-background-muted">
	<div class="se-pre-con uk-flex uk-width-1-1 uk-flex-middle">
			<div class="uk-text-danger uk-width-1-1 uk-text-center" uk-spinner="ratio: 2"></div>
	</div>
		<!-- OFFCANVAS -->
		<div id="offcanvas-nav" data-uk-offcanvas="flip: true; overlay: true">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
				<button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>
				<img src="/images/logo-white.svg" class="uk-margin-top" width="150px" />
				<ul class="uk-nav uk-nav-default uk-margin-top uk-nav-parent-icon" uk-nav="multiple: true">
					<li><a href="/">Home</a></li>
					<?if($_SESSION['user_groups']=='2' || $_SESSION['user_groups']=='1'){?>
					<li class="uk-parent">
						<a href="#">Site Users Page</a>
							<ul class="uk-nav-sub">
								<?
								$userlist = mysqli_query($link,"SELECT user_id, user_login FROM users WHERE user_groups=4 AND user_active=1 ORDER BY user_login ASC");
								while($ulist = mysqli_fetch_array($userlist)){?>
								<li><a href="/su-order?id=<?=$ulist['user_id']?>"><?=$ulist['user_login']?></a></li>
								<?php } mysqli_data_seek($userlist, 0);?>
							</ul> 
					</li>
					<li><a href="/online-order-list">Online orders</a></li>
					<li><a href="/users-list">Users list</a></li>
					<?}?>
					<li><a href="http://visacenter.at">Zurück zur Homepage</a></li>
					<li class="uk-nav-header"><hr/></li>
					<li><a href="/edit-profile"><span data-uk-icon="icon: refresh"></span> Edit profile</a></li>
					<li><a href="?logout"><span data-uk-icon="icon: sign-out"></span> Logout</a></li>
					<!--li class="uk-nav-header">Header</li>
					<li><a href="#js-options"><span class="uk-margin-small-right uk-icon" data-uk-icon="icon: table"></span> Item</a></li>
					<li><a href="#"><span class="uk-margin-small-right uk-icon" data-uk-icon="icon: thumbnails"></span> Item</a></li>
					<li class="uk-nav-divider"></li>
					<li><a href="#"><span class="uk-margin-small-right uk-icon" data-uk-icon="icon: trash"></span> Item</a></li-->
				</ul>
			</div>
		</div>
		<!-- /OFFCANVAS -->
		<!--HEADER-->
		<header class="uk-box-shadow-small" style="background-color: white">
			<div class="uk-container uk-container-expand">
				<nav class="uk-navbar" id="navbar" data-uk-navbar>
					<div class="uk-navbar-left">
						<a class="uk-navbar-item uk-logo" href="/"><img src="/images/logo.svg"/></a>
					</div>
					
					<div class="uk-navbar-right">
						<ul class="uk-navbar-nav uk-visible@m">
							<li><a href="/"><span uk-icon="icon: home; ratio: 0.8;"></span> Dashboard</a></li>
                            <li><a href="/create-document"><span uk-icon="icon: push; ratio: 0.8;"></span> Create Document</a></li>

							<li>
								<a href="#" uk-icon="icon:user; ratio: 0.8;"></a>
								<div class="uk-navbar-dropdown uk-navbar-dropdown-bottom-left">
									<ul class="uk-nav uk-navbar-dropdown-nav">
										<li class="uk-nav-header uk-text-danger">Hi, <?=$_SESSION['user_name']." ".$_SESSION['user_surname']?></li>
										<li class="uk-nav-divider"></li>
										<!--li><a href="/register"><span data-uk-icon="icon: info"></span> Register user</a></li-->
										<!--li><a href="#"><span data-uk-icon="icon: settings"></span> Configuration</a></li>
										<li class="uk-nav-divider"></li>
										<li><a href="#"><span data-uk-icon="icon: image"></span> Your Pictures</a></li-->
										<li><a href="/edit-profile"><span uk-icon="icon: pencil; ratio: 0.8;"></span> Edit profile</a></li>
										<!--li><a href="?logout"><span data-uk-icon="icon: sign-out"></span> Logout</a></li-->
										
									</ul>
								</div>
							</li>
						</ul>
						<div class="uk-navbar-item">
							<a class="uk-navbar-toggle uk-hidden@m" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a>
							<a href="?logout" class="uk-button uk-button-danger uk-border-pill uk-button-small uk-visible@m">Logout <span uk-icon="icon: sign-out; ratio: 0.8;"></span></a>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<!--/HEADER-->
    <div class="main-area">