$(document).ready(function() {
    $(".se-pre-con").fadeOut("slow");
});
jQuery(function() {
	(function($) {
		var bar = $("#progressbaroriginal")[0];
		UIkit.upload(".original-upload", {
			url: "/include/fileupload.php",//put path to your script here f.e. upload.php /include/fileupload.php
			multiple: false,
			allow: "*.pdf",
			name: "file_original",
			beforeSend: function() {
				//console.log("beforeSend", arguments);
			},
			beforeAll: function() {
				//console.log("beforeAll", arguments);
			},
			load: function() {
				//console.log("load", arguments);
			},
			error: function() {
				//console.log("error", arguments);
			},
			complete: function() {
				//console.log("complete", arguments);
			},
			loadStart: function(e) {
				//console.log("loadStart", arguments);
				bar.removeAttribute("hidden");
				bar.max = e.total;
				bar.value = e.loaded;
			},

			progress: function(e) {
				//console.log("progress", arguments);
				bar.max = e.total;
				bar.value = e.loaded;
			},
			loadEnd: function(e) {
				//console.log("loadEnd", arguments);
				bar.max = e.total;
				bar.value = e.loaded;
			},
			completeAll: function() {
				//console.log("completeAll", arguments);
				setTimeout(function() {
					bar.setAttribute("hidden", "hidden");
					//$("#original-upload")[0].classList.add("uk-hidden");
					$(".preview-button-original-file")[0].classList.remove("uk-hidden");
				}, 1000);
				//alert(JSON.parse(arguments[0].response).originalfname);
				$(".original-file-name").html(JSON.parse(arguments[0].response).originalfname);
				$(".uk-link-preview-original-file").attr('href', '/uploads/'+JSON.parse(arguments[0].response).newfname);
				$(".file-original-id").val(JSON.parse(arguments[0].response).id);
				UIkit.notification("PDF upload have been finished", {status:'success'});
			}
		});
	})(jQuery);
	(function($) {
		var bartwo = $("#progressbartranslated")[0];
		UIkit.upload(".translated-upload", {
			url: "/include/fileupload.php",//put path to your script here f.e. upload.php /include/fileupload.php
			multiple: false,
			allow: "*.pdf",
			name: "file_translated",
			beforeSend: function() {
				//console.log("beforeSend", arguments);
			},
			beforeAll: function() {
				//console.log("beforeAll", arguments);
			},
			load: function() {
				//console.log("load", arguments);
			},
			error: function() {
				//console.log("error", arguments);
			},
			complete: function() {
				//console.log("complete", arguments);
			},
			loadStart: function(e) {
				//console.log("loadStart", arguments);
				bartwo.removeAttribute("hidden");
				bartwo.max = e.total;
				bartwo.value = e.loaded;
			},
			progress: function(e) {
				//console.log("progress", arguments);
				bartwo.max = e.total;
				bartwo.value = e.loaded;
			},
			loadEnd: function(e) {
				//console.log("loadEnd", arguments);
				bartwo.max = e.total;
				bartwo.value = e.loaded;
			},
			completeAll: function() {
				//console.log("completeAll", arguments);
				setTimeout(function() {
					bartwo.setAttribute("hidden", "hidden");
					//$("#translated-upload")[0].classList.add("uk-hidden");
					$(".preview-button-translated-file")[0].classList.remove("uk-hidden");
				}, 1000);
				$(".translated-file-name").html(JSON.parse(arguments[0].response).originalfname);
				$(".uk-link-preview-translated-file").attr('href', '/uploads/'+JSON.parse(arguments[0].response).newfname);
				$(".file-translated-id").val(JSON.parse(arguments[0].response).id);
				UIkit.notification("PDF upload have been finished", {status:'success'});
			}
		});
	})(jQuery);

	$("#source-language").selectize({
		create: false,
		sortField: "text",
	});
	$("#target-language").selectize({
		create: false,
		sortField: "text",
	});
	$("#template-country").selectize({
		create: false,
		sortField: "text",
	});

	jQuery('.create-document').validate({
		ignore: [],
		errorClass: 'uk-form-danger',
		errorElement: 'span',
		rules: {
			file_translated_source_language: {
				required: true
			},
			file_translated_target_language: {
				required: true
			},
			company: {
				required: true
			}
		},
		submitHandler: function (form) {
			jQuery.ajax({
				type: "POST",
				url: "/include/createdocument.php",
				data: jQuery(form).serialize(),
				success: function (response) {
					console.log(response);
					jQuery('.create-document')[0].reset();
					setTimeout(function() {
						window.location.href = response;
					}, 1000);
				},
				error: function(response) {
					UIkit.notification(response, {status:'danger'});
				}
			});
		}
	});
	jQuery('.sign-document').validate({
		ignore: [],
		errorClass: 'uk-form-danger',
		errorElement: 'span',
		rules: {},
		submitHandler: function (form) {
			jQuery.ajax({
				type: "POST",
				url: "/include/signdocument.php",
				data: jQuery(form).serialize(),
				success: function (response) {
					console.log(response);
					jQuery('.sign-document')[0].reset();
					setTimeout(function() {
						window.location.href = response;
					}, 1000);
				},
				error: function(response) {
					UIkit.notification(response, {status:'danger'});
				}
			});
		}
	});



	jQuery('.loginform').validate({
		errorClass: 'uk-form-danger',
		errorElement: 'div',
		rules: {
		},
		submitHandler: function (form) {
			jQuery.ajax({
				type: "POST",
				url: "/include/login.php",
				data: jQuery(form).serialize(),
				success: function (response) {
					if(response.replace(/\s/g, "") == "check"){
						location.reload();
					}else{
						UIkit.notification(response, {status:'danger'});
					}
				},
				error: function(response) {
					UIkit.notification(response, {status:'danger'});
				}
			});
		}
	});


	/*---------datepicker----------*/
      var $startDate = $('.start-date');
      var $startDate2 = $('.start-date2');
      var $endDate = $('.end-date');
      var $endDate2 = $('.end-date2');

      $startDate.datepicker({
        autoHide: true,
		format: 'dd/mm/yyyy',
      });
	  $startDate2.datepicker({
        autoHide: true,
		format: 'dd/mm/yyyy',
      });
      $endDate.datepicker({
        autoHide: true,
		format: 'dd/mm/yyyy',
        startDate: $startDate.datepicker('getDate'),
      });
	  $endDate2.datepicker({
        autoHide: true,
		format: 'dd/mm/yyyy',
        startDate: $startDate.datepicker('getDate'),
      });

      $startDate.on('change', function () {
        $endDate.datepicker('setStartDate', $startDate.datepicker('getDate'));
      });
	  
	  $startDate2.on('change', function () {
        $endDate2.datepicker('setStartDate', $startDate2.datepicker('getDate'));
      });
	  $('[data-toggle="datepicker"]').datepicker({
		  autoHide: true,
		  format: 'dd/mm/yyyy',
		});
	
	
	
	/*jQuery('.adduserform').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'div',
		rules: {
			mail: { 
                    email: true
            },
			cfmMail: { 
                    equalTo: "#mail"
            }
        },
        submitHandler: function (form) {
			jQuery.ajax({
                 type: "POST",
                 url: "/include/adduser.php",
                 data: jQuery(form).serialize(),
                 success: function (response) {
					UIkit.notification(response, {status:'danger'});
					location.reload();
                 },
				 error: function(response) {
                    UIkit.notification(response, {status:'danger'});
                }
             });
         }
    });*/
	
	jQuery('.sucreateorder').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'span',
		rules: {
			mail: { 
                    email: true
            },
			cfmMail: { 
                    equalTo: "#mail"
            }
        },
        submitHandler: function (form) {
			jQuery.ajax({
                 type: "POST",
                 url: "/include/sucreateorder.php",
                 data: jQuery(form).serialize(),
                 success: function (response) {
					$(".sucreateorder")[0].reset();
					location.href = '/';
					UIkit.notification(response, {status:'danger'});
                 },
				 error: function(response) {
                    UIkit.notification(response, {status:'danger'});
                }
             });
         }
    });
	
	jQuery('.sucreateorder-files').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'span',
		rules: {
			mail: { 
                    email: true
            },
			cfmMail: { 
                    equalTo: "#mail"
            }
        },
    });
	
	jQuery('.sucreateorderk5-files').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'span',
    });
	
	
	
	jQuery('.sucreateemployee-files').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'span',
    });
	
	jQuery('.suupdateemployee-files').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'span',
    });
	
	jQuery('.suupdateorder-files').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'span',
    });
	
	
	 jQuery('.update-pwd').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'div',
		rules: {
			cnfPwd: { 
                    equalTo: "#pwd"
            }
        },
        submitHandler: function (form) {
			jQuery.ajax({
                 type: "POST",
                 url: "/include/updatepwd.php",
                 data: jQuery(form).serialize(),
                 success: function (response) {
						UIkit.notification(response, {status:'success'});
						$(".update-pwd")[0].reset();
						UIkit.modal('#edit-password').hide();						
                 },
				 error: function(response) {
                    UIkit.notification(response, {status:'danger'});
                }
             });
         }
    });
	
	

	
	jQuery('.forgot-pwd').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'div',
		rules: {
        },
        submitHandler: function (form) {
			jQuery.ajax({
                 type: "POST",
                 url: "/include/forgotpwd.php",
                 data: jQuery(form).serialize(),
                 success: function (response) {
					alert(response);
					/* if(response.replace(/\s/g, "") == "check"){	
						location.reload();
					}else{
						UIkit.notification(response, {status:'danger'});
					}  */

                 },
				 error: function(response) {
                    UIkit.notification(response, {status:'danger'});
                }
             });
         }
    });
	

	
	jQuery('.update-user').validate({
        errorClass: 'uk-form-danger',
		errorElement: 'div',
		rules: {
			mail: { 
                    email: true
            }
        },
        submitHandler: function (form) {
			jQuery.ajax({
                 type: "POST",
                 url: "/include/updateuser.php",
                 data: jQuery(form).serialize(),
                 success: function (response) {
					UIkit.notification(response, {status:'danger'});
					/* location.reload(true); */

                 },
				 error: function(response) {
                    UIkit.notification(response, {status:'danger'});
                }
             });
         }
    });
	
	
	
	$('select.select-groups').on('change', function() {
	  var dataName = $('select.select-groups').find(':selected').data('name');
	  if(dataName == "manager"){
		  $('.price-user-block').removeClass( "uk-hidden");
		  UIkit.update();
	  }else{
		  $('.price-user-block').addClass( "uk-hidden");
		  UIkit.update();
	  }
	});
	
	$('select.select-type-visa').on('change', function() {
	  var dataName = $('select.select-type-visa').find(':selected').data('name');
	  if(dataName == "t"){
		 $('.tour-visa-gr').addClass("uk-hidden");
		  $('.tour-visa').removeClass("uk-hidden");
		  UIkit.update();
	  }else if(dataName == "tg"){
		  $('.tour-visa').addClass( "uk-hidden");
		  $('.tour-visa-gr').removeClass( "uk-hidden");
		  UIkit.update();
	  }
	});
	
	$('select.select-type-bvisa').on('change', function() {
	  var dataName = $('select.select-type-bvisa').find(':selected').data('name');
	  if(dataName == "fms"){
		  $('.company-visa-type').addClass("uk-hidden");
		  $('.work-visa-type').addClass("uk-hidden");
		  $('.fms-visa-type').removeClass("uk-hidden");
		  UIkit.update();
	  }else if(dataName == "company"){
		  $('.company-visa-type').removeClass("uk-hidden");
		  $('.work-visa-type').addClass("uk-hidden");
		  $('.fms-visa-type').addClass("uk-hidden");
		  UIkit.update();
	  }else if(dataName == "work"){
		  $('.company-visa-type').addClass("uk-hidden");
		  $('.work-visa-type').removeClass("uk-hidden");
		  $('.fms-visa-type').addClass("uk-hidden");
		  UIkit.update();
	  }else{
		  $('.company-visa-type').addClass("uk-hidden");
		  $('.work-visa-type').addClass("uk-hidden");
		  $('.fms-visa-type').addClass("uk-hidden");
		  UIkit.update();
	  }
	});
	
	$('select.select-reiseland').on('change', function() {
	  var dataName = $('select.select-reiseland').find(':selected').data('name');
	  if(dataName == "Russia"){
		  $('.stadt-block').addClass("uk-hidden");
		  $('.taxi-block').removeClass("uk-hidden");
		  $('.russian-city-block').removeClass("uk-hidden");
		  UIkit.update();
	  }else if(dataName != "Russia"){
		  $('.stadt-block').removeClass("uk-hidden");
		  $('.taxi-block').addClass("uk-hidden");
		  $('.russian-city-block').addClass("uk-hidden");
		  UIkit.update();
	  }
	});
	
	$('input[type=radio][name=n52]').change(function() {
		if (this.value == '1') {
			$('.taxi-detail-block').removeClass("uk-hidden");
			UIkit.update();
		}
		else{
			$('.taxi-detail-block').addClass("uk-hidden");
			UIkit.update();
		}
	});
	
	$('#zustelladresse').click(function() {
		  if (this.checked) {
				var firma = $("#firma").val();
				$( "#firma2" ).val(firma);
				var tel = $("#tel").val();
				$( "#tel2" ).val(tel);
				var strasse = $("#strasse").val();
				$( "#strasse2" ).val(strasse);
				var plz = $("#plz").val();
				$( "#plz2" ).val(plz);
				var ort = $("#ort").val();
				$( "#ort2" ).val(ort);
				}
				 if (!this.checked) {
					$( "#firma2" ).val('');
					$( "#tel2" ).val('');
					$( "#strasse2" ).val('');
					$( "#plz2" ).val('');
					$( "#ort2" ).val('');
				}
		});
		
	/*$('.edit-file-emp').click(function() {
		var filesName = $(this).data('filename');
		$('.'+filesName+'-file').removeClass("uk-hidden");
	});	*/
				 
});